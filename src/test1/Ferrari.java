package test1;

public class Ferrari extends Car {
	private String name;
	// 1.
	// public Ferrari()	{
	//	super();
	// }
	
	// 2.
	// public Ferrari(String name) {
	//  this.name = name;
	// }
	
	// 3.
	// public Ferrari() {
	// }
	
	// public Ferrari(String name) {
	// passing parameter = error
	// }
	
	// 4.	
	public Ferrari()	{
		String name = "Ferrari";
		System.out.println(name);
	}
	
}

	

