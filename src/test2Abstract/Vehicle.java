package test2Abstract;

public abstract class Vehicle {
	private String type;
	
	public Vehicle(String type) {
		this.type = type;
	}
	
	public abstract int Speed(); // ��������� abstract �з�����Դ syntax error
	
	public String toString()	{
		return this.type;
	}
}
