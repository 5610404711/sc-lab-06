package test2Abstract;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Motorcycle test1 = new Motorcycle("Kawasaki");
		Car test2 = new Car("Bugatti");

		System.out.println(test1);
		System.out.println(test2);
		
		ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();
		vehicles.add(test1);
		vehicles.add(test2);
		
		for (Vehicle i : vehicles)	{
			System.out.println("Speed (km/hr): "+i.Speed());
		}
		}

}
