package test3Shadow;

public class Main {
	
	public static void main(String[] args) {
		
		Car car = new Car("Bugatti");
		System.out.println(car.speedField()); 
		System.out.println(car.speedLocal());
		
		
		Motorcycle motor = new Motorcycle("BigBike");
		System.out.println(motor.speedField()); 
		System.out.println(motor.speedLocal());
		
	}

}
