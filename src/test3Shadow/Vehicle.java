package test3Shadow;

public abstract class Vehicle {
	private String type;
	
	public Vehicle(String type) {
		this.type = type;
	}
	
	public abstract int Speed(); 
	
	public String toString()	{
		return this.type;
	}
}